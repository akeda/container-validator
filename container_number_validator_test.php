<?php
require_once('../simpletest/unit_tester.php');
require_once('test_reporter.php');
require_once('container_number_validator.php');

class TestOfContainerNumberValidator extends UnitTestCase {
    var $validator;
    var $start = 0;
    var $time = 0;
    var $end = 0;
    
    function TestOfContainerNumberValidator() {
        $this->UnitTestCase('ContainerNumberValidator Test');
        $this->validator = new ContainerNumberValidator;
    }
    
    function setUp() {
        $this->start = microtime(true);
    }
    
    function tearDown() {
        $this->end = microtime(true);
        $this->time += $this->end - $this->start;
        echo "\n" . ($this->end - $this->start) . " seconds<hr />";
    }
    
    function testIsValid() {
        $result = $this->validator->isValid('TEXU3070079');
        $this->assertTrue( $result );
        
        $result = $this->validator->isValid('TEXU3070077');
        $this->assertFalse( $result );
    }
    
    function testValidate() {
        $result = $this->validator->validate('TEXU3070079');
        $expected = array('TEXU3070079', 'TEX', 'U', '307007', '9');
        $this->assertIdentical( $result, $expected );
        
        $result = $this->validator->validate('TEXU3070077');
        $expected = array();
        $this->assertIdentical( $result, $expected );
    }
    
    function testCreateCheckDigit() {
        $result = $this->validator->createCheckDigit('TEXU307007');
        $expected = 9;
        $this->assertEqual($result, $expected);
    }
    
    function testGenerate() {
        $result = $this->validator->generate('TEX', 'U', 0, 99);
        $this->assertEqual( count($result), 100 );
        $this->assertIdentical( $result[0],  'TEXU0000000');
        $this->assertIdentical( $result[99], 'TEXU0000993');
        
        $result = $this->validator->generate('TEX', 'U', 999997, 999999);
        $this->assertEqual( count($result), 3 );
        $this->assertIdentical( $result[999997], 'TEXU9999976' );
        $this->assertIdentical( $result[999998], 'TEXU9999981' );
        $this->assertIdentical( $result[999999], 'TEXU9999997' );
        
        foreach ( $result as $container ) {
            $this->assertEqual( substr( $container, -1 ), $this->validator->createCheckDigit( substr($container, 0, -1) ) );
        }
    }
    
    function testGetErrorMessages() {
        $this->validator->validate('TEXU3070077');
        $result = $this->validator->getErrorMessages();
        $expected = array('Check digit is not match');
        $this->assertIdentical( $result, $expected );
        
        $this->validator->validate('');
        $result = $this->validator->getErrorMessages();
        $expected = array('Container number must be a string');
        $this->assertIdentical( $result, $expected );
        
        $myValidator = new ContainerNumberValidator;
        $myValidator->getOwnerCode();
        $myValidator->getProductGroupCode();
        $myValidator->getRegistrationDigit();
        $myValidator->getCheckDigit();
        $result = $myValidator->getErrorMessages();
        $expected = array('You must call validate or isValid first','You must call validate or isValid first','You must call validate or isValid first', 'You must call validate or isValid first');
        $this->assertIdentical( $result, $expected );
        
        $this->validator->clearErrors();
        $this->validator->generate('EP', 'ZZ', 0, 1);
        $result = $this->validator->getErrorMessages();
        $expected = 'Invalid owner code or product group code';
        $this->assertIdentical( $result[0], $expected );
        
        $this->validator->clearErrors();
        $this->validator->generate('EPX', 'U', -1, 2);
        $result = $this->validator->getErrorMessages();
        $expected = 'Invalid number to generate, minimal is 0 and maximal is 999999';
        $this->assertIdentical( $result[0], $expected );
        
        $this->validator->clearErrors();
        $this->validator->generate('EPX', '9', 1, 2);
        $result = $this->validator->getErrorMessages();
        $expected = array('Invalid container number', 'Error generating container number at number 1');
        $this->assertIdentical( $result, $expected );
    }
}

$test = &new TestOfContainerNumberValidator();
$test->run(new TestReporter());
echo $test->time . " seconds";
?>