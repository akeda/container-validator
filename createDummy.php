<?php
require_once('container_number_validator.php');
$validator = new ContainerNumberValidator;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Warehouse Integrated Management System :: Imp Obs</title>
</head>
<body>
<div id="container">
<h1>Valid Container Number Generator</h1>
<?php
if ( empty($_POST['data']) ) { ?>
    <form enctype="application/x-www-form-urlencoded" method="post" name="form" id="form">
        <p>
        <label for="owner_code">Owner Code (3 character)</label>
        <input type="text" name="data[owner_code]" id="owner_code" maxlength="3" size="3" />
        </p>
        
        <p>
        <label for="product_group_code">Product Group Code (1 character)</label>
        <input type="text" name="data[product_group_code]" id="product_group_code" maxlength="3" size="1"/>
        </p>
        
        <p>
        <label for="from">From</label>
        <input type="text" name="data[from]" id="from" maxlength="6" size="6" />
        </p>
        
        <p>
        <label for="to">To</label>
        <input type="text" name="data[to]" id="to" maxlength="6" size="6" />
        </p>
        
        <input type="submit" name="submit" value="Generate" />
    </form>
    <?php
} else { // data being posted
    $owner_code = $_POST['data']['owner_code'];
    $product_group_code = $_POST['data']['product_group_code'];
    $from = $_POST['data']['from'];
    $to = $_POST['data']['to'];
    
    $validator = new ContainerNumberValidator;
    $containers = $validator->generate( $owner_code, $product_group_code, $from, $to );
    $errors = $validator->getErrorMessages();
    
    echo '<a href="" title="generate again">Generate again</a><br />';
    if ( empty($errors) ) {
        
        foreach ( $containers as $container ) {
            echo $container . '<br />';
        }
    } else {
        $inflector = ( count($errors) > 1 ) ? 'There are some errors!' : 'There is an error!';
        echo "<h3>$inflector</h3>";
        echo '<ul>';
        foreach ( $errors as $error ) {
            echo '<li>' . $error . '</li>';
        }
        echo '</ul>';
    }
}
?>
</div>
</body>
</html>